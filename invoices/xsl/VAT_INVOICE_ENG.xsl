<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0" xmlns:fo="http://www.w3.org/1999/XSL/Format"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/Invoice">
        <fo:root xmlns:fo="http://www.w3.org/1999/XSL/Format" font-family="Calibri">

            <fo:layout-master-set>
                <fo:simple-page-master master-name="main"
                                       page-height="29.7cm"
                                       page-width="21.0cm"
                                       margin-top="1cm"
                                       margin-bottom="1cm"
                                       margin-left="2cm"
                                       margin-right="2cm">
                    <fo:region-body margin-bottom="1cm"/>
                    <fo:region-after extent="0.5cm"/>
                </fo:simple-page-master>
            </fo:layout-master-set>

            <fo:page-sequence master-reference="main">
                <fo:static-content flow-name="xsl-region-after">
                    <fo:block font-size="7pt" text-align="right">
                        Page
                        <fo:page-number/>
                        of
                        <fo:page-number-citation ref-id="last-page"/>
                    </fo:block>
                </fo:static-content>
                <fo:flow flow-name="xsl-region-body">
                    <fo:table table-layout="fixed" width="100%" font-size="8pt">
                        <fo:table-column column-width="50mm"/>
                        <fo:table-column column-width="80mm"/>
                        <fo:table-column column-width="40mm"/>
                        <fo:table-body>
                            <fo:table-row>
                                <fo:table-cell column-number="1" number-columns-spanned="2">
                                    <fo:block>
                                        <fo:external-graphic
                                                src='{Logo}'
                                                content-height="90px"
                                                content-width="scale-to-fit" scaling="non-uniform"
                                        />
                                    </fo:block>
                                </fo:table-cell>

                                <fo:table-cell column-number="3" padding-top="5mm" padding-bottom="1cm">
                                    <fo:block font-weight="bold" padding="0.5mm">
                                        Issue place:
                                    </fo:block>
                                    <fo:block>
                                        <xsl:value-of select="IssueDetails/IssuePlace"/>
                                    </fo:block>
                                    <fo:block font-weight="bold" padding="0.5mm">
                                        Issue date:
                                    </fo:block>
                                    <fo:block>
                                        <xsl:value-of select="IssueDetails/IssueDate"/>
                                    </fo:block>
                                    <fo:block font-weight="bold" padding="0.5mm">
                                        Payment due:
                                    </fo:block>
                                    <fo:block>
                                        <xsl:value-of select="IssueDetails/PaymentDate"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row>
                                <fo:table-cell column-number="1">
                                    <fo:block font-weight="bold" padding="0.5mm">
                                        Seller:
                                    </fo:block>
                                    <fo:block padding="0.2mm">
                                        <xsl:value-of select="Seller/Name"/>
                                    </fo:block>
                                    <fo:block padding="0.2mm">
                                        <xsl:value-of select="Seller/Street"/>
                                    </fo:block>
                                    <fo:block padding="0.2mm">
                                        <xsl:value-of select="Seller/PostalCode"/>
                                    </fo:block>
                                    <fo:block padding="0.2mm">
                                        NIP:
                                        <xsl:value-of select="Seller/TaxId"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="3">
                                    <fo:block font-weight="bold" padding="0.5mm">
                                        Buyer:
                                    </fo:block>
                                    <fo:block padding="0.2mm">
                                        <xsl:value-of select="Buyer/Name"/>
                                    </fo:block>
                                    <fo:block padding="0.2mm">
                                        <xsl:value-of select="Buyer/Street"/>
                                    </fo:block>
                                    <fo:block padding="0.2mm">
                                        <xsl:value-of select="Buyer/PostalCode"/>
                                    </fo:block>
                                    <fo:block padding="0.2mm">
                                        NIP:
                                        <xsl:value-of select="Buyer/TaxId"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-body>
                    </fo:table>
                    <fo:block font-size="15pt" text-align="center" font-weight="bold" space-before="8mm"
                              space-after="5mm">
                        VAT Invoice
                        <xsl:value-of select="Number"/>
                    </fo:block>
                    <fo:table table-layout="fixed" width="100%" font-size="8pt" display-align="center">
                        <fo:table-column column-width="8mm"/>
                        <fo:table-column column-width="55mm"/>
                        <fo:table-column column-width="10mm"/>
                        <fo:table-column column-width="12mm"/>
                        <fo:table-column column-width="15mm"/>
                        <fo:table-column column-width="10mm"/>
                        <fo:table-column column-width="20mm"/>
                        <fo:table-column column-width="20mm"/>
                        <fo:table-column column-width="20mm"/>
                        <fo:table-body>
                            <fo:table-row text-align="center" background-color="Gainsboro">
                                <fo:table-cell column-number="1" border-width="0.2mm" border-style="solid"
                                               padding="1mm">
                                    <fo:block>
                                        On
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="2" border-width="0.2mm" border-style="solid"
                                               padding="1mm">
                                    <fo:block>
                                        Product
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="3" border-width="0.2mm" border-style="solid"
                                               padding="1mm">
                                    <fo:block>
                                        Quantity
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="4" border-width="0.2mm" border-style="solid"
                                               padding="1mm">
                                    <fo:block>
                                        Unit
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="5" border-width="0.2mm" border-style="solid"
                                               padding="1mm">
                                    <fo:block>
                                        Nett price
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="6" border-width="0.2mm" border-style="solid"
                                               padding="1mm">
                                    <fo:block>
                                        VAT [%]
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="7" border-width="0.2mm" border-style="solid"
                                               padding="1mm">
                                    <fo:block>
                                        Nett value
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="8" border-width="0.2mm" border-style="solid"
                                               padding="1mm">
                                    <fo:block>
                                        VAT
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="9" border-width="0.2mm" border-style="solid"
                                               padding="1mm">
                                    <fo:block>
                                        Gross value
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <xsl:for-each select="Products/Product">
                                <fo:table-row>
                                    <fo:table-cell column-number="1" border-width="0.2mm" border-style="solid"
                                                   padding="1mm">
                                        <fo:block>
                                            <xsl:value-of select="Number"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell column-number="2" border-width="0.2mm" border-style="solid"
                                                   padding="1mm">
                                        <fo:block>
                                            <xsl:value-of select="Name"/>
                                        </fo:block>
                                        <fo:block>
                                            <xsl:value-of select="Description"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell text-align="center" column-number="3" border-width="0.2mm"
                                                   border-style="solid" padding="1mm">
                                        <fo:block>
                                            <xsl:value-of select="Quantity"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell text-align="center" column-number="4" border-width="0.2mm"
                                                   border-style="solid" padding="1mm">
                                        <fo:block>
                                            <xsl:value-of select="Unit"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell text-align="right" column-number="5" border-width="0.2mm"
                                                   border-style="solid" padding="1mm">
                                        <fo:block>
                                            <xsl:value-of select="Price"/>
                                            <xsl:value-of select="Currency"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell text-align="center" column-number="6" border-width="0.2mm"
                                                   border-style="solid" padding="1mm">
                                        <fo:block>
                                            <xsl:value-of select="VatRate"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell text-align="right" column-number="7" border-width="0.2mm"
                                                   border-style="solid" padding="1mm">
                                        <fo:block>
                                            <xsl:value-of select="NettValue"/>
                                            <xsl:value-of select="Currency"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell text-align="right" column-number="8" border-width="0.2mm"
                                                   border-style="solid" padding="1mm">
                                        <fo:block>
                                            <xsl:value-of select="VatValue"/>
                                            <xsl:value-of select="Currency"/>
                                        </fo:block>
                                    </fo:table-cell>
                                    <fo:table-cell text-align="right" column-number="9" border-width="0.2mm"
                                                   border-style="solid" padding="1mm">
                                        <fo:block>
                                            <xsl:value-of select="GrossValue"/>
                                            <xsl:value-of select="Currency"/>
                                        </fo:block>
                                    </fo:table-cell>
                                </fo:table-row>
                            </xsl:for-each>
                        </fo:table-body>
                    </fo:table>
                    <fo:table table-layout="fixed" width="100%" font-size="8pt" display-align="center"
                              space-before="5mm"
                              space-after="7mm">
                        <fo:table-column column-width="proportional-column-width(1)"/>
                        <fo:table-column column-width="62mm"/>
                        <fo:table-column column-width="20mm"/>
                        <fo:table-column column-width="20mm"/>
                        <fo:table-column column-width="20mm"/>
                        <fo:table-body>
                            <fo:table-row text-align="center" background-color="Gainsboro">
                                <fo:table-cell column-number="2" border-width="0.2mm" border-style="solid"
                                               padding="1mm">
                                    <fo:block>
                                        By VAT rate
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="3" border-width="0.2mm" border-style="solid"
                                               padding="1mm">
                                    <fo:block>
                                        Nett value
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="4" border-width="0.2mm" border-style="solid"
                                               padding="1mm">
                                    <fo:block>
                                        VAT amount
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="5" border-width="0.2mm" border-style="solid"
                                               padding="1mm">
                                    <fo:block>
                                        Gross value
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row>
                                <fo:table-cell column-number="2" border-width="0.2mm" border-style="solid"
                                               padding="1mm" text-align="center">
                                    <fo:block>
                                        Basic VAT tax
                                        <xsl:value-of select="Products/Product/VatRate"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="3" border-width="0.2mm" border-style="solid"
                                               padding="1mm" text-align="right">
                                    <fo:block>
                                        <xsl:value-of select="Values/TotalNettValue"/><xsl:value-of select="Products/Product/Currency"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="4" border-width="0.2mm" border-style="solid"
                                               padding="1mm" text-align="right">
                                    <fo:block>
                                        <xsl:value-of select="Values/TotalVatValue"/> <xsl:value-of select="Products/Product/Currency"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="5" border-width="0.2mm" border-style="solid"
                                               padding="1mm" text-align="right">
                                    <fo:block>
                                        <xsl:value-of select="Values/TotalGrossValue"/> <xsl:value-of select="Products/Product/Currency"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row>
                                <fo:table-cell column-number="2" padding="1mm" text-align="right">
                                    <fo:block>
                                        Total:
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="3" padding="1mm" text-align="right">
                                    <fo:block>
                                        <xsl:value-of select="Values/TotalNettValue"/> <xsl:value-of select="Products/Product/Currency"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="4" padding="1mm" text-align="right">
                                    <fo:block>
                                        <xsl:value-of select="Values/TotalVatValue"/> <xsl:value-of select="Products/Product/Currency"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="5" padding="1mm" text-align="right">
                                    <fo:block>
                                        <xsl:value-of select="Values/TotalGrossValue"/> <xsl:value-of select="Products/Product/Currency"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-body>
                    </fo:table>
                    <fo:table table-layout="fixed" width="100%" font-size="8pt" display-align="center"
                              space-before="7mm"
                              space-after="6mm">
                        <fo:table-column column-width="proportional-column-width(1)"/>
                        <fo:table-column column-width="32mm"/>
                        <fo:table-column column-width="90mm"/>
                        <fo:table-body text-align="right">
                            <fo:table-row background-color="Gainsboro">
                                <fo:table-cell column-number="2" padding="1mm" border-width="0.2mm"
                                               border-style="solid">
                                    <fo:block>
                                        Total to pay:
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="3" padding="1mm" border-width="0.2mm"
                                               border-style="solid">
                                    <fo:block>
                                        <xsl:value-of select="Values/TotalGrossValue"/> <xsl:value-of select="Products/Product/Currency"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row >
                                <fo:table-cell column-number="2" padding="1mm">
                                    <fo:block>
                                        In words:
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="3" padding="1mm">
                                    <fo:block>
                                        <xsl:value-of select="Values/AmountInWords"/> <xsl:value-of select="Products/Product/Currency"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-body>
                    </fo:table>
                    <fo:table table-layout="fixed" width="100%" font-size="8pt" display-align="center"
                              space-before="6mm"
                              space-after="7mm">
                        <fo:table-column column-width="30mm"/>
                        <fo:table-column column-width="60mm"/>
                        <fo:table-column column-width="proportional-column-width(1)"/>
                        <fo:table-body>
                            <fo:table-row>
                                <fo:table-cell column-number="1" padding="0.2mm">
                                    <fo:block>
                                        Payment terms:
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="2" padding="0.2mm">
                                    <fo:block>
                                        <xsl:value-of select="Details/PaymentTerms"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row>
                                <fo:table-cell column-number="1" padding="0.2mm">
                                    <fo:block>
                                        Payment method:
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="2" padding="0.2mm">
                                    <fo:block>
                                        <xsl:value-of select="Details/PaymentMethod"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row>
                                <fo:table-cell column-number="1" padding="0.2mm">
                                    <fo:block>
                                        Payment due:
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="2" padding="0.2mm">
                                    <fo:block>
                                        <xsl:value-of select="IssueDetails/IssueDate"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row>
                                <fo:table-cell column-number="1" padding="0.2mm">
                                    <fo:block>
                                        Bank:
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="2" padding="0.2mm">
                                    <fo:block>
                                        <xsl:value-of select="Details/BankName"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row>
                                <fo:table-cell column-number="1" padding="0.2mm">
                                    <fo:block>
                                        Account number:
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="2" padding="0.2mm">
                                    <fo:block>
                                        <xsl:value-of select="Details/AccountNumber"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row>
                                <fo:table-cell column-number="1" padding="0.2mm">
                                    <fo:block>
                                        BIC/SWIFT :
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="2" padding="0.2mm">
                                    <fo:block>
                                        <xsl:value-of select="Details/SWIFT"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row>
                                <fo:table-cell column-number="1" padding-top="1.8mm" font-weight="bold">
                                    <fo:block>
                                        COMMENTS:
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row>
                                <fo:table-cell column-number="1" padding="0.2mm" number-columns-spanned="2">
                                    <fo:block>
                                        <xsl:value-of select="Details/Comments"/>
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-body>
                    </fo:table>
                    <fo:table table-layout="fixed" width="100%" font-size="8pt" display-align="center"
                              space-before="7mm"
                              space-after="2mm"
                              text-align="center">
                        <fo:table-column column-width="50mm"/>
                        <fo:table-column column-width="proportional-column-width(1)"/>
                        <fo:table-column column-width="50mm"/>
                        <fo:table-body>
                            <fo:table-row>
                                <fo:table-cell column-number="1" background-color="Gainsboro"
                                               padding="0.5mm" font-weight="bold" border-width="0.2mm" border-style="solid">
                                    <fo:block>
                                        Issued by:
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="3" background-color="Gainsboro"
                                               padding="0.5mm" font-weight="bold" border-width="0.2mm" border-style="solid">
                                    <fo:block>
                                        Received by:
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row>
                                <fo:table-cell column-number="1" padding="0.5mm" padding-top="17mm" border-width="0.2mm" border-style="solid">
                                    <fo:block>
                                        <xsl:value-of select="Details/IssuedBy"/>
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="3" padding="0.5mm" padding-top="17mm" border-width="0.2mm" border-style="solid">
                                    <fo:block>

                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                            <fo:table-row font-size="5pt">
                                <fo:table-cell column-number="1" padding="0.5mm">
                                    <fo:block>
                                        Signature of the person authorized to issue a VAT backend.invoice
                                    </fo:block>
                                </fo:table-cell>
                                <fo:table-cell column-number="3" padding="0.5mm">
                                    <fo:block>
                                        Signature of the person authorized to receive VAT backend.invoice
                                    </fo:block>
                                </fo:table-cell>
                            </fo:table-row>
                        </fo:table-body>
                    </fo:table>
                    <fo:block id="last-page"/>
                </fo:flow>
            </fo:page-sequence>
        </fo:root>
    </xsl:template>


</xsl:stylesheet>