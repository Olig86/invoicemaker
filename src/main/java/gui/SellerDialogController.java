package gui;

import backend.Global;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class SellerDialogController {

    @FXML
    private AnchorPane sellerDialog;

    @FXML
    private TextField sellerName;

    @FXML
    private TextField sellerStreet;

    @FXML
    private TextField sellerPostalCode;

    @FXML
    private TextField sellerTaxId;

    @FXML
    private Button ok;

    public void initialize() {
        ok.setDisable(true);
    }

    public void checkFieldsStatus() {
        if(
                (!sellerName.getText().isEmpty()) &&
                        (!sellerStreet.getText().isEmpty()) &&
                        (!sellerPostalCode.getText().isEmpty()) &&
                        (!sellerTaxId.getText().isEmpty())
                ) {
            ok.setDisable(false);
        }
        else {
            ok.setDisable(true);
        }
    }

    public void processResults() {
        Global.coordinator.createSeller(sellerName.getText(),
                sellerStreet.getText(),
                sellerPostalCode.getText(),
                sellerTaxId.getText());
    }

    public void closeWindow(){
        Stage stage = (Stage) sellerDialog.getScene().getWindow();
        stage.close();
    }

    public void confirmChanges() {
        processResults();
        closeWindow();
    }

}
