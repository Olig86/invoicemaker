package gui;

import backend.Coordinator;
import backend.Global;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class BuyerDialogController {

    @FXML
    private TextField buyerName;

    @FXML
    private TextField buyerStreet;

    @FXML
    private TextField buyerPostalCode;

    @FXML
    private TextField buyerTaxId;

    @FXML
    private AnchorPane buyerDialog;

    @FXML
    private Button ok;

    public void initialize() {
        ok.setDisable(true);
    }

    public void processResults() {
        Global.coordinator.createBuyer(buyerName.getText(),
                buyerStreet.getText(),
                buyerPostalCode.getText(),
                buyerTaxId.getText());
    }

    public void checkFieldsStatus() {
        if(
                (!buyerName.getText().isEmpty()) &&
                        !buyerPostalCode.getText().isEmpty() &&
                        !buyerStreet.getText().isEmpty() &&
                        !buyerTaxId.getText().isEmpty()
                ) {
            ok.setDisable(false);
        }
        else {
            ok.setDisable(true);
        }
    }

    public void closeWindow(){
        Stage stage = (Stage) buyerDialog.getScene().getWindow();
        stage.close();
    }

    public void confirmChanges() {
        processResults();
        closeWindow();
    }
}
