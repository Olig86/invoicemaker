package gui;

import backend.Coordinator;
import backend.Global;
import backend.invoice.Currency;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

public class BankDialogController {

    @FXML
    private AnchorPane bankDialog;

    @FXML
    private ChoiceBox<Currency> currencyChoice;

    @FXML
    private TextField bankName;

    @FXML
    private TextField accountNumber;

    @FXML
    private TextField swift;

    @FXML
    private Button ok;

    public void initialize() {
        currencyChoice.getItems().addAll(Currency.values());
        ok.setDisable(true);
    }

    public void checkFieldsStatus() {
        if(
                (!bankName.getText().isEmpty()) &&
                        !accountNumber.getText().isEmpty() &&
                        !swift.getText().isEmpty() &&
                        (currencyChoice.getValue() != null)
                ) {
            ok.setDisable(false);
        }
        else {
            ok.setDisable(true);
        }
    }

    public void processResults() {
        Global.coordinator.createBankAccount(bankName.getText(),
                accountNumber.getText(),
                currencyChoice.getSelectionModel().getSelectedItem(),
                swift.getText());
    }

    public void closeWindow(){
        Stage stage = (Stage) bankDialog.getScene().getWindow();
        stage.close();
    }

    public void confirmChanges() {
        processResults();
        closeWindow();
    }
}
