package gui;

import backend.Coordinator;
import backend.Global;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;


public class ProductDialogController {

    @FXML
    private AnchorPane productDialog;

    @FXML
    private TextField productName;

    @FXML
    private TextField productPrice;

    @FXML
    private TextField taxRate;

    @FXML
    private TextField unit;

    @FXML
    private TextArea description;

    @FXML
    private Button ok;

    public void initialize() {
        ok.setDisable(true);
        productPrice.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d{0,7}([\\.]\\d{0,2})?")) {
                productPrice.setText(oldValue);
            }
        });
        taxRate.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("\\d{0,2}?")) {
                taxRate.setText(oldValue);
            }
        });
    }

    public void checkFieldsStatus() {
        if(
                (!productName.getText().isEmpty()) &&
                        !productPrice.getText().isEmpty() &&
                        !taxRate.getText().isEmpty() &&
                        !unit.getText().isEmpty()
                ) {
            ok.setDisable(false);
        }
        else {
            ok.setDisable(true);
        }
    }

    public void processResults() {

        if(!productPrice.getText().isEmpty() && !taxRate.getText().isEmpty()) {
            Global.coordinator.createProduct(productName.getText(),
                    description.getText(),
                    Double.parseDouble(productPrice.getText()),
                    Integer.parseInt(taxRate.getText()),
                    unit.getText());
        }
    }

    public void closeWindow(){
        Stage stage = (Stage) productDialog.getScene().getWindow();
        stage.close();
    }

    public void confirmChanges() {
        processResults();
        closeWindow();
    }
}
