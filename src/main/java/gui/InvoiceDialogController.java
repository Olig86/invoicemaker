package gui;

import backend.Coordinator;
import backend.Global;
import backend.invoice.BankAccount;
import backend.invoice.Company;
import backend.invoice.InvoiceType;
import backend.invoice.Product;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.*;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.IOException;
import java.time.LocalDate;
import java.util.Optional;

public class InvoiceDialogController {

    @FXML
    private Button addToInvoice;

    @FXML
    private TextField invoiceNumber;

    @FXML
    private TextField issuePlace;

    @FXML
    private TextField paymentTerms;

    @FXML
    private TextField paymentMethod;

    @FXML
    private TextField invoiceDescription;

    @FXML
    private TextField issuedBy;

    @FXML
    private TextField productQuantity;

    @FXML
    private ChoiceBox<InvoiceType> invoiceType;

    @FXML
    private ChoiceBox<BankAccount> accountNumber;

    @FXML
    private ChoiceBox<Company> chooseClient;

    @FXML
    private ChoiceBox<Product> chooseProduct;

    @FXML
    private DatePicker paymentDate;

    @FXML
    private DatePicker issueDate;

    @FXML
    private AnchorPane invoiceAnchorPane;

    @FXML
    private Button ok;

    private Coordinator coordinator = Global.coordinator;

    public void initialize() {
        ok.setDisable(true);
        addToInvoice.setDisable(true);
        issuePlace.setText("Warszawa");
        issueDate.setValue(LocalDate.now());
        paymentDate.setValue(LocalDate.now().plusDays(7));
        invoiceType.getItems().addAll(InvoiceType.values());
        accountNumber.getItems().addAll(coordinator.getBankAccounts());
        paymentTerms.setText("przedpłata");
        paymentMethod.setText("przelew");
        chooseClient.getItems().addAll(coordinator.getBuyerList());
        issuedBy.setText("Olivier Głowacki");
        chooseProduct.getItems().addAll(coordinator.getProducts());
    }

    @FXML
    public void showProductWindow() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(invoiceAnchorPane.getScene().getWindow());
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("ProductDialog.fxml"));
        try {
            dialog.getDialogPane().setContent(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        Optional<ButtonType> result = dialog.showAndWait();
        if(!result.isPresent()) {
            chooseProduct.getItems().clear();
            chooseProduct.getItems().addAll(coordinator.getProducts());
            int lastProduct = chooseProduct.getItems().size() - 1;
            chooseProduct.setValue(chooseProduct.getItems().get(lastProduct));
        }
    }

    @FXML
    public void showBuyerWindow() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(invoiceAnchorPane.getScene().getWindow());
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("BuyerDialog.fxml"));
        try {
            dialog.getDialogPane().setContent(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        Optional<ButtonType> result = dialog.showAndWait();
        if(!result.isPresent()) {
            chooseClient.getItems().clear();
            chooseClient.getItems().addAll(coordinator.getBuyerList());
            int lastProduct = chooseClient.getItems().size() - 1;
            chooseClient.setValue(chooseClient.getItems().get(lastProduct));
        }

    }

    @FXML
    private void addToInvoice() {
        coordinator.addInvoiceProduct(
                chooseProduct.getValue(),
                Integer.parseInt(productQuantity.getText())
        );
        productQuantity.clear();
        checkFieldsStatus();
//        ObservableList<Product> data = FXCollections.observableArrayList(chooseProduct.getValue());
    }

    public void processResults() {
        coordinator.createInvoice(
                invoiceNumber.getText(),
                invoiceType.getValue(),
                issuePlace.getText(),
                issueDate.getValue(),
                paymentDate.getValue(),
                chooseClient.getValue(),
                paymentTerms.getText(),
                paymentMethod.getText(),
                accountNumber.getValue(),
                invoiceDescription.getText(),
                issuedBy.getText()
        );
        try {
            coordinator.saveInvoice();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void checkFieldsStatus() {
        if(
                !invoiceNumber.getText().isEmpty() &&
                        (invoiceType.getValue() != null) &&
                        (accountNumber.getValue() != null) &&
                        !issuePlace.getText().isEmpty() &&
                        !paymentTerms.getText().isEmpty() &&
                        (chooseClient.getValue() != null) &&
                        !issuedBy.getText().isEmpty() &&
                        !coordinator.getInvoiceProducts().isEmpty() &&
                       (coordinator.getSeller() != null)
                ) {
            ok.setDisable(false);
        }
        else {
            ok.setDisable(true);
        }
    }

    public void checkProductFields() {
        if((chooseProduct.getValue() != null) &&
                !productQuantity.getText().isEmpty()
                ) {
            addToInvoice.setDisable(false);
        }
        else {
            addToInvoice.setDisable(true);
        }
    }

    public void closeWindow(){
        coordinator.getInvoiceProducts().clear();
        Stage stage = (Stage) invoiceAnchorPane.getScene().getWindow();
        stage.close();
    }

    public void confirmChanges() {
        processResults();
        closeWindow();
    }
}
