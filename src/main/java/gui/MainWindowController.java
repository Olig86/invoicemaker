package gui;

import backend.Coordinator;
import backend.Global;
import backend.invoice.Company;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TableView;
import javafx.scene.layout.AnchorPane;

import java.io.IOException;
import java.util.Optional;

public class MainWindowController {

    @FXML
    private AnchorPane mainAnchorPane;

    @FXML
    TableView<Company> buyers;

    @FXML
    private Button showSellerDialog;

    @FXML
    private Button showBankDialog;

    @FXML
    private Button showBuyerDialog;

    @FXML
    private Button showProductDialog;

    @FXML
    private Button showInvoiceDialog;

    private Coordinator coordinator = Global.coordinator;

    public void initialize() {
        if (
                (coordinator.getSeller() == null) ||
                        !coordinator.getBankAccounts().isEmpty()
                ) {
            showBuyerDialog.setDisable(true);
            showInvoiceDialog.setDisable(true);
            showProductDialog.setDisable(true);
        }
    }

    public void checkSellerStatus() {
        if(
                (coordinator.getSeller() != null) &&
                !coordinator.getBankAccounts().isEmpty()
                ) {
            showBuyerDialog.setDisable(false);
            showInvoiceDialog.setDisable(false);
            showProductDialog.setDisable(false);
        }
        else {
            showBuyerDialog.setDisable(true);
            showInvoiceDialog.setDisable(true);
            showProductDialog.setDisable(true);
        }
    }

    @FXML
    public void showBankWindow() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(mainAnchorPane.getScene().getWindow());
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("BankDialog.fxml"));
        try {
            dialog.getDialogPane().setContent(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        Optional<ButtonType> result = dialog.showAndWait();
        if(!result.isPresent()) {
            checkSellerStatus();
        }
    }

    @FXML
    public void showSellerWindow() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(mainAnchorPane.getScene().getWindow());
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("SellerDialog.fxml"));
        try {
            dialog.getDialogPane().setContent(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        Optional<ButtonType> result = dialog.showAndWait();
        if(!result.isPresent()) {
            checkSellerStatus();
        }
    }

    @FXML
    public void showBuyerWindow() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(mainAnchorPane.getScene().getWindow());
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("BuyerDialog.fxml"));
        try {
            dialog.getDialogPane().setContent(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        dialog.showAndWait();
    }

    @FXML
    public void showProductWindow() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(mainAnchorPane.getScene().getWindow());
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("ProductDialog.fxml"));
        try {
            dialog.getDialogPane().setContent(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        dialog.showAndWait();
    }

    @FXML
    public void showInvoiceWindow() {
        Dialog<ButtonType> dialog = new Dialog<>();
        dialog.initOwner(mainAnchorPane.getScene().getWindow());
        FXMLLoader loader = new FXMLLoader();
        loader.setLocation(getClass().getResource("InvoiceDialog.fxml"));
        try {
            dialog.getDialogPane().setContent(loader.load());
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }
        dialog.showAndWait();
    }

}
