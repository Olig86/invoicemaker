import backend.Coordinator;
import backend.Global;
import backend.invoice.Currency;
import backend.invoice.InvoiceType;

import java.time.LocalDate;

public class Simulation {


    public static void main(String[] args) throws Exception {

        Coordinator coordinator = Global.coordinator;

        coordinator.createSeller("Krati sp. z o.o.", "Al. Jerozolimskie 81",
                "02-001 Warszawa", "7010472595");

        coordinator.createBuyer("Przykładowa firma 3", "Stefana Bryły 3/439",
                "02-685 Warszawa", "0000000000");

        coordinator.createProduct("pendrive", "przykładowy opis pendrive stworzony dla mega super testu"
                , 40.23, 0, "sztuka");

        coordinator.createProduct("power bank", "", 34.50, 23, "sztuka");

        coordinator.createProduct("pendrive", "przykładowy opis pendrive stworzony dla mega super testu"
                , 34.60, 23, "sztuka");

        coordinator.createProduct("power bank", "", 22.40, 23, "sztuka");

        coordinator.addInvoiceProduct(coordinator.getProducts().get(0),50);
        coordinator.addInvoiceProduct(coordinator.getProducts().get(1),300);
        coordinator.addInvoiceProduct(coordinator.getProducts().get(1),1500);




        coordinator.createBankAccount("mBank", "02 0000 0000 0000 0000 0000 0000",
                Currency.PLN, "sdfsg");

        coordinator.createInvoice("03K-01-2018", InvoiceType.VAT_INVOICE_PL, "Warszawa",
                LocalDate.of(2018, 1, 3), LocalDate.of(2018, 1, 5),
                coordinator.getBuyerList().get(0),"przedplata", "przelew",
                coordinator.getBankAccounts().get(0),"testowy komentarz do faktury", "Olivier Głowacki");

        coordinator.saveInvoice();

    }
}
