package backend.invoice;

public class Product {

    private String name;
    private String description;
    private Double price;
    private int vatRate;
    private String unit;
    private int quantity;
    private double nettValue;
    private double vatValue;
    private double grossValue;

    public Product(String name, String description, double price, int vatRate, String unit) {
        this.name = name;
        this.description = description;
        this.price = price;
        this.vatRate = vatRate;
        this.unit = unit;
    }

    public void calculateValues() {
        nettValue = quantity*price;
        vatValue = nettValue*vatRate/100;
        grossValue = nettValue + vatValue;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public double getPrice() {
        return price;
    }

    public int getVatRate() {
        return vatRate;
    }

    public String getUnit() {
        return unit;
    }

    public int getQuantity() {
        return quantity;
    }

    public double getNettValue() {
        return nettValue;
    }

    public double getVatValue() {
        return vatValue;
    }

    public double getGrossValue() {
        return grossValue;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String toString() {
        return name;
    }
}
