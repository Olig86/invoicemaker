package backend.invoice;

public enum Currency {
    PLN("zł"), EUR("€");

    String name;

    Currency(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
