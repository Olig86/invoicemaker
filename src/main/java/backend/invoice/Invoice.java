package backend.invoice;

import backend.invoice.numbers.converter.EnglishNumberToWords;
import backend.invoice.numbers.converter.PolishNumberToWords;

import java.time.LocalDate;
import java.util.List;

public class Invoice {

    private String number;
    private InvoiceType invoiceType;
    private String issuePlace;
    private LocalDate issueDate;
    private LocalDate paymentDate;
    private Company buyer;
    private Company seller;
    private List<Product> productList;
    private String paymentTerms;
    private String paymentMethod;
    private BankAccount bankdetails;
    private String comments;
    private String issuedByEmploye;
    private double totalNettValue;
    private double totalGrossValue;
    private double totalVatValue;
    private String amountInWords;

    public Invoice(String number, InvoiceType invoiceType, String issuePlace, LocalDate issueDate, LocalDate paymentDate,
                   Company buyer, Company seller, List<Product> productList, String paymentTerms,
                   String paymentMethod, BankAccount bankDetails, String comments, String issuedByEmploye) {
        this.number = number;
        this.invoiceType = invoiceType;
        this.issuePlace = issuePlace;
        this.issueDate = issueDate;
        this.paymentDate = paymentDate;
        this.buyer = buyer;
        this.seller = seller;
        this.productList = productList;
        this.paymentTerms = paymentTerms;
        this.paymentMethod = paymentMethod;
        this.bankdetails = bankDetails;
        this.comments = comments;
        this.issuedByEmploye = issuedByEmploye;
        calculateTotalValues();
        convertAmountToWords();
    }

    private void convertAmountToWords() {
        if(invoiceType == InvoiceType.VAT_INVOICE_PL
                || invoiceType == InvoiceType.ADVANCE_INVOICE_PL
                || invoiceType == InvoiceType.FINAL_INVOICE_PL
                || invoiceType == InvoiceType.PROFORMA_INVOICE_PL) {
            amountInWords = PolishNumberToWords.convert(totalGrossValue);
        }
        else {
            amountInWords = EnglishNumberToWords.convert(totalGrossValue);
        }
    }

    public void calculateTotalValues() {
        for (Product product : productList) {
            totalNettValue += product.getNettValue();
            totalGrossValue += product.getGrossValue();
            totalVatValue += product.getVatValue();
        }
    }

    public String getNumber() {
        return number;
    }

    public InvoiceType getInvoiceType() {
        return invoiceType;
    }

    public String getIssuePlace() {
        return issuePlace;
    }

    public LocalDate getIssueDate() {
        return issueDate;
    }

    public LocalDate getPaymentDate() {
        return paymentDate;
    }

    public Company getBuyer() {
        return buyer;
    }

    public Company getSeller() {
        return seller;
    }

    public List<Product> getProductList() {
        return productList;
    }

    public String getPaymentTerms() {
        return paymentTerms;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public BankAccount getBankdetails() {
        return bankdetails;
    }

    public String getComments() {
        return comments;
    }

    public String getIssuedByEmploye() {
        return issuedByEmploye;
    }

    public double getTotalNettValue() {
        return totalNettValue;
    }

    public double getTotalGrossValue() {
        return totalGrossValue;
    }

    public double getTotalVatValue() {
        return totalVatValue;
    }

    public String getAmountInWords() {
        return amountInWords;
    }
}
