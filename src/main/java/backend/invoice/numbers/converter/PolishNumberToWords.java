package backend.invoice.numbers.converter;

import java.math.RoundingMode;
import java.text.DecimalFormat;

public class PolishNumberToWords {

    private static final String[] hundNames = {
            "",
            " sto",
            " dwieście",
            " trzysta",
            " czterysta",
            " pięćset",
            " sześćset",
            " siedemset",
            " osiemset",
            " dziewięćset"
    };

    private static final String[] tensNames = {
            "",
            " dziesięć",
            " dwadzieścia",
            " trzydzieści",
            " czterdzieści",
            " pięćdziesiąt",
            " sześćdziesiąt",
            " siedemdziesiąt",
            " osiemdziesiąt",
            " dziewięćdziesiąt"
    };

    private static final String[] numNames = {
            "",
            " jeden",
            " dwa",
            " trzy",
            " cztery",
            " pięć",
            " sześć",
            " siedem",
            " osiem",
            " dziewięć",
            " dziesięć",
            " jedenaście",
            " dwanaście",
            " trzynaście",
            " czternaście",
            " piętnaście",
            " szesnaście",
            " siedemnaście",
            " osiemnaście",
            " dziewiętnaście"
    };

    private PolishNumberToWords() {
    }

    private static String convertLessThanOneThousand(int number) {

        String soFar;

        if (number % 100 < 20) {
            soFar = numNames[number % 100];
            number /= 100;
            return hundNames[number] + soFar;
        } else {
            soFar = numNames[number % 10];
            number /= 10;

            soFar = tensNames[number % 10] + soFar;
            number /= 10;
        }
        if (number == 0) return soFar;
        return hundNames[number] + soFar;
    }


    public static String convert(Double number) {

        double ending = (number - number.intValue())*100;
        String peanuts = String.format(" %.0f/100",ending);


        // 0 to 999 999 999 999
        if (number == 0) {
            return "zero";
        }

        String snumber = Double.toString(number);

        // pad with "0"
        String mask = "000000000000";
        DecimalFormat df = new DecimalFormat(mask);
        df.setRoundingMode(RoundingMode.DOWN);
        snumber = df.format(number);

        // XXXnnnnnnnnn
        int billions = Integer.parseInt(snumber.substring(0, 3));
        // nnnXXXnnnnnn
        int millions = Integer.parseInt(snumber.substring(3, 6));
        // nnnnnnXXXnnn
        int hundredThousands = Integer.parseInt(snumber.substring(6, 9));
        // nnnnnnnnnXXX
        int thousands = Integer.parseInt(snumber.substring(9, 12));

        String tradBillions;
        switch (billions) {
            case 0:
                tradBillions = "";
                break;
            case 1:
                tradBillions = convertLessThanOneThousand(billions)
                        + " miliard";
                break;
            case 2: case 3: case 4:
                tradBillions = convertLessThanOneThousand(billions)
                        + " miliardy";
                break;
            default:
                if (billions%10 == 2
                        || billions%10 == 3
                        || billions%10 == 4) {
                    tradBillions = convertLessThanOneThousand(billions)
                            + " miliardy";
                }
                else {
                    tradBillions = convertLessThanOneThousand(billions)
                            + " miliardów";
                }
                break;
        }
        String result = tradBillions;

        String tradMillions;
        switch (millions) {
            case 0:
                tradMillions = "";
                break;
            case 1:
                tradMillions = convertLessThanOneThousand(millions)
                        + " million";
                break;
            case 2: case 3: case 4:
                tradMillions = convertLessThanOneThousand(millions)
                        + " milliony";
                break;
            default:
                if (millions%10 == 2
                        || millions%10 == 3
                        || millions%10 == 4) {
                    tradMillions = convertLessThanOneThousand(millions)
                            + " milliony";
                }
                else {
                    tradMillions = convertLessThanOneThousand(millions)
                            + " millionów";
                }
                break;
        }
        result = result + tradMillions;

        String tradHundredThousands;
        switch (hundredThousands) {
            case 0:
                tradHundredThousands = "";
                break;
            case 1:
                tradHundredThousands = "tysiąc";
                break;
            case 2: case 3: case 4:
                tradHundredThousands = convertLessThanOneThousand(hundredThousands)
                        + " tysiące";
                break;
            default:
                if (hundredThousands%10 == 2
                        || hundredThousands%10 == 3
                        || hundredThousands%10 == 4) {
                    tradHundredThousands = convertLessThanOneThousand(hundredThousands)
                            + " tysiące";
                }
                else {
                    tradHundredThousands = convertLessThanOneThousand(hundredThousands)
                            + " tysięcy";
                }
                break;
        }
        result = result + tradHundredThousands;

        String tradThousand = "";
        tradThousand = convertLessThanOneThousand(thousands);
        result = result + tradThousand;

        // remove extra spaces!
        if(peanuts.equals(" 0/100")) {
            return result.replaceAll("^\\s+", "").replaceAll("\\b\\s{2,}\\b", " ");

        }
        else {
            return result.replaceAll("^\\s+", "").replaceAll("\\b\\s{2,}\\b", " ").concat(peanuts);
        }
    }
}
