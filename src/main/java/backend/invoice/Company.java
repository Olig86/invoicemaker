package backend.invoice;

public class Company {

    private String name;
    private String street;
    private String postalCode;
    private String taxId;

    public Company(String name, String street, String postalCode, String taxId) {
        this.name = name;
        this.street = street;
        this.postalCode = postalCode;
        this.taxId = taxId;
    }

    public String getName() {
        return name;
    }

    public String getStreet() {
        return street;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public String getTaxId() {
        return taxId;
    }

    @Override
    public String toString() {
        return name;
    }
}
