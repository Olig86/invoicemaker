package backend.invoice;

public class BankAccount {

    private String bankName;
    private String number;
    private String swift;
    private Currency currency;

    public BankAccount(String bankName, String number, Currency currency, String swift) {
        this.bankName = bankName;
        this.number = number;
        this.currency = currency;
        this.swift = swift;
    }

    public String getBankName() {
        return bankName;
    }

    public String getNumber() {
        return number;
    }

    public Currency getCurrency() {
        return currency;
    }

    public String getSwift() {
        return swift;
    }

    @Override
    public String toString() {
        return number;
    }
}
