package backend.data.storage;

import backend.invoice.BankAccount;
import backend.invoice.Company;

import java.util.List;

public interface DataStorage {

    void addSeller(Company seller);
    void addBuyer(Company buyer);
    void addBankAccount(BankAccount account);
    Company getSeller();
    List getBuyers();
    List getProducts();
    List getBankAccounts();
}
