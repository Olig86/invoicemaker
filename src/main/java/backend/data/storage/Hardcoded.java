package backend.data.storage;

import backend.invoice.BankAccount;
import backend.invoice.Company;
import backend.invoice.Product;

import java.util.ArrayList;
import java.util.List;

public class Hardcoded implements DataStorage {

    private Company seller;
    private List<Company> buyers;

    @Override
    public List getBankAccounts() {
        return accounts;
    }

    private List<BankAccount> accounts;
    private List<Product> products;

    public Hardcoded() {
        accounts = new ArrayList<>();
        buyers = new ArrayList<>();
        products = new ArrayList<>();
    }

    @Override
    public void addSeller(Company seller) {
        this.seller = seller;
    }

    @Override
    public void addBuyer(Company buyer) {
        buyers.add(buyer);
    }

    @Override
    public void addBankAccount( BankAccount account) {
        accounts.add(account);
    }

    @Override
    public Company getSeller() {
        return seller;
    }

    @Override
    public List getBuyers() {
        return buyers;
    }

    @Override
    public List getProducts() {
        return products;
    }

}
