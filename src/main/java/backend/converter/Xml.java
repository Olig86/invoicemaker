package backend.converter;

import backend.invoice.Company;
import backend.invoice.Invoice;
import backend.invoice.Product;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.nio.file.Path;
import java.util.Formatter;

public class Xml {

    private Xml() {
    }

    public static void createXml(Invoice invoice, Path homePath) throws Exception {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();
        Document document = documentBuilder.newDocument();

        Element rootElement = document.createElement("Invoice");
        document.appendChild(rootElement);

        Element number = document.createElement("Number");
        number.appendChild(document.createTextNode(invoice.getNumber()));
        rootElement.appendChild(number);

        Element logo = document.createElement("Logo");
        String logoPath = "url(\"Images/krati.jpg\")";
        logo.appendChild(document.createTextNode(logoPath));
        rootElement.appendChild(logo);

        createIssueField(invoice, document, rootElement);
        createSellerField(invoice, document, rootElement);
        createBuyerField(invoice, document, rootElement);
        createProductsTable(invoice, document, rootElement);
        createTotalValuesField(invoice, document, rootElement);
        createDeatailsField(invoice, document, rootElement);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        DOMSource source = new DOMSource(document);


        Path xmlPath = homePath.resolve("xml");
        File xmlFolder = new File(xmlPath.toUri());
        File xmlFile = new File(xmlFolder, invoice.getNumber().concat(".xml"));
        StreamResult streamResult = new StreamResult(xmlFile);
        transformer.transform(source, streamResult);
    }

    private static void createDeatailsField(Invoice invoice, Document document, Element rootElement) {
        Element details = document.createElement("Details");
        rootElement.appendChild(details);

        Element paymentTerms = document.createElement("PaymentTerms");
        paymentTerms.appendChild((document.createTextNode(invoice.getPaymentTerms())));
        details.appendChild(paymentTerms);

        Element paymentMethod = document.createElement("PaymentMethod");
        paymentMethod.appendChild((document.createTextNode(invoice.getPaymentMethod())));
        details.appendChild(paymentMethod);

        Element bankName = document.createElement("BankName");
        bankName.appendChild((document.createTextNode(invoice.getBankdetails().getBankName())));
        details.appendChild(bankName);

        Element accountNumber = document.createElement("AccountNumber");
        accountNumber.appendChild((document.createTextNode(invoice.getBankdetails().getNumber())));
        details.appendChild(accountNumber);

        Element swift = document.createElement("SWIFT");
        swift.appendChild((document.createTextNode(invoice.getBankdetails().getSwift())));
        details.appendChild(swift);

        Element comments = document.createElement("Comments");
        comments.appendChild((document.createTextNode(invoice.getComments())));
        details.appendChild(comments);

        Element issuedBy = document.createElement("IssuedBy");
        issuedBy.appendChild((document.createTextNode(invoice.getIssuedByEmploye())));
        details.appendChild(issuedBy);
    }

    private static void createTotalValuesField(Invoice invoice, Document document, Element rootElement) {
        Element values = document.createElement("Values");
        rootElement.appendChild(values);

        Element totalNettValue = document.createElement("TotalNettValue");
        totalNettValue.appendChild((document.createTextNode(addSeparator(invoice.getTotalNettValue()))));
        values.appendChild(totalNettValue);

        Element totalGrossValue = document.createElement("TotalGrossValue");
        totalGrossValue.appendChild((document.createTextNode(addSeparator(invoice.getTotalGrossValue()))));
        values.appendChild(totalGrossValue);

        Element totalVatValue = document.createElement("TotalVatValue");
        totalVatValue.appendChild((document.createTextNode(addSeparator(invoice.getTotalVatValue()))));
        values.appendChild(totalVatValue);

        Element amountInWords = document.createElement("AmountInWords");
        amountInWords.appendChild((document.createTextNode(invoice.getAmountInWords())));
        values.appendChild(amountInWords);
    }

    private static String addSeparator(double value) {
        StringBuilder stringBuilder = new StringBuilder();
        Formatter formatter = new Formatter(stringBuilder);
        formatter.format("%(,.2f", value);
        return stringBuilder.toString();
    }

    private static void createBuyerField(Invoice invoice, Document document, Element rootElement) {
        Company company = invoice.getBuyer();

        Element seller = document.createElement("Buyer");
        rootElement.appendChild(seller);

        Element name = document.createElement("Name");
        name.appendChild(document.createTextNode(company.getName()));
        seller.appendChild(name);

        Element street = document.createElement("Street");
        street.appendChild(document.createTextNode(company.getStreet()));
        seller.appendChild(street);

        Element postalCode = document.createElement("PostalCode");
        postalCode.appendChild(document.createTextNode(company.getPostalCode()));
        seller.appendChild(postalCode);

        Element taxId = document.createElement("TaxId");
        taxId.appendChild(document.createTextNode(company.getTaxId()));
        seller.appendChild(taxId);
    }

    private static void createSellerField(Invoice invoice, Document document, Element rootElement) {
        Company company = invoice.getSeller();

        Element seller = document.createElement("Seller");
        rootElement.appendChild(seller);

        Element name = document.createElement("Name");
        name.appendChild(document.createTextNode(company.getName()));
        seller.appendChild(name);

        Element street = document.createElement("Street");
        street.appendChild(document.createTextNode(company.getStreet()));
        seller.appendChild(street);

        Element postalCode = document.createElement("PostalCode");
        postalCode.appendChild(document.createTextNode(company.getPostalCode()));
        seller.appendChild(postalCode);

        Element taxId = document.createElement("TaxId");
        taxId.appendChild(document.createTextNode(company.getTaxId()));
        seller.appendChild(taxId);
    }

    private static void createIssueField(Invoice invoice, Document document, Element rootElement) {
        Element dates = document.createElement("IssueDetails");
        rootElement.appendChild(dates);

        Element issuePlace = document.createElement("IssuePlace");
        issuePlace.appendChild(document.createTextNode(invoice.getIssuePlace()));
        dates.appendChild(issuePlace);

        Element issueDate = document.createElement("IssueDate");
        issueDate.appendChild(document.createTextNode(invoice.getIssueDate().toString()));
        dates.appendChild(issueDate);

        Element paymentDate = document.createElement("PaymentDate");
        paymentDate.appendChild(document.createTextNode(invoice.getPaymentDate().toString()));
        dates.appendChild(paymentDate);
    }

    private static void createProductsTable(Invoice invoice, Document document, Element rootElement) {
        Element products = document.createElement("Products");
        rootElement.appendChild(products);

        int i = 1;
        for (Product item: invoice.getProductList()) {

            Element product = document.createElement("Product");
            products.appendChild(product);

            Element currency = document.createElement("Currency");
                        currency.appendChild(document.createTextNode(" ".concat(invoice.getBankdetails().getCurrency().getName())));
            product.appendChild(currency);

            Element number = document.createElement("Number");
            number.appendChild(document.createTextNode(String.valueOf(i)));
            product.appendChild(number);

            Element name = document.createElement("Name");
            name.appendChild(document.createTextNode(item.getName()));
            product.appendChild(name);

            Element description = document.createElement("Description");
            description.appendChild(document.createTextNode(item.getDescription()));
            product.appendChild(description);

            Element quantity = document.createElement("Quantity");
            quantity.appendChild(document.createTextNode(String.valueOf(item.getQuantity())));
            product.appendChild(quantity);

            Element unit = document.createElement("Unit");
            unit.appendChild(document.createTextNode(item.getUnit()));
            product.appendChild(unit);

            Element Price = document.createElement("Price");
            Price.appendChild(document.createTextNode(addSeparator(item.getPrice())));
            product.appendChild(Price);

            Element vatRate = document.createElement("VatRate");
            vatRate.appendChild(document.createTextNode(String.format("%s%%", item.getVatRate())));
            product.appendChild(vatRate);

            Element nettValue = document.createElement("NettValue");
            nettValue.appendChild(document.createTextNode(addSeparator(item.getNettValue())));
            product.appendChild(nettValue);

            Element vatValue = document.createElement("VatValue");
            vatValue.appendChild(document.createTextNode(addSeparator(item.getVatValue())));
            product.appendChild(vatValue);

            Element grossValue = document.createElement("GrossValue");
            grossValue.appendChild(document.createTextNode(addSeparator(item.getGrossValue())));
            product.appendChild(grossValue);

            i++;
        }
    }
}
