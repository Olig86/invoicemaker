package backend.converter;

import backend.invoice.Invoice;
import org.apache.fop.apps.FOUserAgent;
import org.apache.fop.apps.Fop;
import org.apache.fop.apps.FopFactory;
import org.apache.fop.apps.MimeConstants;
import org.xml.sax.SAXException;

import javax.xml.transform.Result;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.sax.SAXResult;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Path;

public class Converter {

    private static Path xmlPath;
    private static Path xslPath;
    private static Path pdfPath;
    private static Path foPath;
    private static Path homePath;

    private Converter(){
    }

    private static void createPaths(Invoice invoice) {
            xmlPath = homePath.resolve("xml").resolve(invoice.getNumber().concat(".xml"));
            xslPath = homePath.resolve("xsl").resolve(invoice.getInvoiceType().toString().concat(".xsl"));
            pdfPath = homePath.resolve("pdf").resolve(invoice.getNumber().concat(".pdf"));
            foPath = homePath.resolve("fo").resolve(invoice.getNumber().concat(".fo"));
    }

    public static void convertToPDF(Invoice invoice, Path path) {
        homePath = path;
        createPaths(invoice);
        convertToFO();
        try {
            File xsltFile = new File(xslPath.toUri());
            StreamSource xmlSource = new StreamSource(new File(xmlPath.toUri()));
            Path confPath = homePath.toAbsolutePath().getParent().resolve("fop.conf");
//            String path = projectPath.concat("\\fop.conf");
            FopFactory fopFactory = FopFactory.newInstance(new File(confPath.toUri()));
            FOUserAgent foUserAgent = fopFactory.newFOUserAgent();
            OutputStream out;
            out = new FileOutputStream(pdfPath.toString());
            Fop fop = fopFactory.newFop(MimeConstants.MIME_PDF, foUserAgent, out);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer(new StreamSource(xsltFile));
            Result res = new SAXResult(fop.getDefaultHandler());


            transformer.transform(xmlSource, res);
            out.close();
        } catch (TransformerException | IOException | SAXException e) {
            e.printStackTrace();
        }
    }

    private static void convertToFO() {

        try {
            OutputStream out = new FileOutputStream(foPath.toString());
            File xsltFile = new File(xslPath.toUri());
            StreamSource xmlSource = new StreamSource(new File(xmlPath.toUri()));
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer(new StreamSource(xsltFile));
            Result res = new StreamResult(out);
            transformer.transform(xmlSource, res);
            out.close();
        } catch (TransformerException | IOException e) {
            e.printStackTrace();
        }
    }
}