package backend;

import backend.converter.Converter;
import backend.converter.Xml;
import backend.data.storage.DataStorage;
import backend.data.storage.Hardcoded;
import backend.invoice.*;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Coordinator {


    private Company seller;
    private Company buyer;
    private Invoice invoice;
    private Path homePath = FileSystems.getDefault().getPath("invoices");
    private BankAccount bankAccount;
    private DataStorage dataStorage = new Hardcoded();
    private List invoiceProducts = new ArrayList<>();

    public void addInvoiceProduct(Product product, int quantity) {
        product.setQuantity(quantity);
        product.calculateValues();
        invoiceProducts.add(product);
    }

    public void createSeller (String name, String street, String postalCode, String taxId) {
        seller = new Company(name, street, postalCode, taxId);
        dataStorage.addSeller(seller);
    }

    public void createBuyer (String name, String street, String postalCode, String taxId) {
        buyer = new Company(name, street, postalCode, taxId);
        dataStorage.addBuyer(buyer);
    }

    public void createProduct(String name, String description, double price, int vatRate, String unit) {
        Product product = new Product(name, description, price, vatRate, unit);
        dataStorage.getProducts().add(product);
    }

    public void createBankAccount(String bankName, String number, Currency currency, String swift) {
        bankAccount = new BankAccount(bankName, number, currency, swift);
        dataStorage.addBankAccount(bankAccount);
    }

    public void createInvoice(String number, InvoiceType invoiceType, String issuePlace, LocalDate issueDate,
                                     LocalDate paymentDate, Company buyer, String paymentTerms, String paymentMethod,
                                     BankAccount bankAccount, String comments, String issuedByEmploye) {
        invoice = new Invoice(number, invoiceType, issuePlace, issueDate, paymentDate, buyer, seller, invoiceProducts, paymentTerms,
                paymentMethod, bankAccount, comments, issuedByEmploye);
    }

    public List getInvoiceProducts() {
        return invoiceProducts;
    }

    public void saveInvoice() throws Exception {
        Xml.createXml(invoice, homePath);
        Converter.convertToPDF(invoice, homePath);

        if (Desktop.isDesktopSupported()) {
            try {
                Path pdfPath = homePath.resolve("pdf");
                File pdfFolder = new File(pdfPath.toUri());
                File pdfFile = new File(pdfFolder, invoice.getNumber().concat(".pdf"));
                Desktop.getDesktop().open(pdfFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public Company getSeller() {
        return dataStorage.getSeller();
    }

    public List<Company> getBuyerList() {
        return dataStorage.getBuyers();
    }

    public List<BankAccount> getBankAccounts() {
        return dataStorage.getBankAccounts();
    }

    public List<Product> getProducts() {
        return dataStorage.getProducts();
    }
}
