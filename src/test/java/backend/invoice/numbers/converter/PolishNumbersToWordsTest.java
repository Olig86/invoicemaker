package backend.invoice.numbers.converter;

import org.junit.Assert;
import org.junit.Test;

public class PolishNumbersToWordsTest {

    @Test
    public void shouldReturn_tysiąc() {
        Double number = 1000.00;
        System.out.println(PolishNumberToWords.convert(number));
        Assert.assertTrue(PolishNumberToWords.convert(number).equals("tysiąc"));
    }


    @Test
    public void shouldReturn_dwa_tysiące() {
        Double number = 2000.00;
        Assert.assertTrue(PolishNumberToWords.convert(number).equals("dwa tysiące"));
    }

    @Test
    public void shouldReturn_trzy_tysiące() {
        Double number = 3000.00;
        Assert.assertTrue(PolishNumberToWords.convert(number).equals("trzy tysiące"));
    }

    @Test
    public void shouldReturn_cztery_tysiące_trzysta_dwadzieścia_jeden() {
        Double number = 4321.00;
        Assert.assertTrue(PolishNumberToWords.convert(number).equals("cztery tysiące trzysta dwadzieścia jeden"));
    }

    @Test
    public void shouldReturn_pięć_tysięcy() {
        Double number = 5000.00;
        Assert.assertTrue(PolishNumberToWords.convert(number).equals("pięć tysięcy"));
    }

    @Test
    public void shouldReturn_dwadzieścia_pięć_tysięcy_osiemset_siedemnaście() {
        Double number = 25817.00;
        Assert.assertTrue(PolishNumberToWords.convert(number).equals("dwadzieścia pięć tysięcy osiemset siedemnaście"));
    }

    @Test
    public void shouldReturn_sto_tysięcy() {
        Double number = 100000.00;
        Assert.assertTrue(PolishNumberToWords.convert(number).equals("sto tysięcy"));
    }

    @Test
    public void shouldReturn_sto_trzydzieści_dwa_tysiące_trzysta_czterdzieści_osiem_20_100() {
        Double number = 132348.20;
        Assert.assertTrue(PolishNumberToWords.convert(number).equals("sto trzydzieści dwa tysiące trzysta czterdzieści osiem 20/100"));
    }

    @Test
    public void shouldReturn_sto_trzydzieści_trzy_tysiące_trzysta_czterdzieści_osiem_17_100() {
        Double number = 133348.17;
        Assert.assertTrue(PolishNumberToWords.convert(number).equals("sto trzydzieści trzy tysiące trzysta czterdzieści osiem 17/100"));
    }

    @Test
    public void shouldReturn_dwieście_osiemdziesiąt_pięć_tysięcy() {
        Double number = 285000.00;
        Assert.assertTrue(PolishNumberToWords.convert(number).equals("dwieście osiemdziesiąt pięć tysięcy"));
    }

    @Test
    public void shouldReturn_pięćset_dwa_tysiące() {
        Double number = 502000.00;
        Assert.assertTrue(PolishNumberToWords.convert(number).equals("pięćset dwa tysiące"));
    }
}
